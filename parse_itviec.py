import urllib.request
import redis
import requests
from bs4 import BeautifulSoup
from requests import Session
from Database_connector import JobRepository
from Job import Job
from util import load_config

Config = load_config()

r = redis.Redis(host=Config['redis']['host'], port=Config['redis']['port'], db=Config['redis']['db'])

domain = "https://itviec.com"

    # with Session() as s:
    #     site = s.get("https://itviec.com/sign_in")
    #     bs_content = BeautifulSoup(site.content, "html.parser")
    #     token = bs_content.find("meta", {"name":"tracking-token"})["content"]
    #     login_data = {"email":"phamhien193@outlook.com","password":"anhhien193", "csrf_token":token}
    #     s.post("https://itviec.com/sign_in",login_data)
    #     home_page = s.get("https://itviec.com")
    #     print(home_page.content)


# your_page = requests.get("https://itviec.com/it-jobs/sr-nodejs-backend-dev-javascript-3000-travala-com-4409", auth=
# ('phamhien193@outlook.com', 'anhhien193'))
# soup_res = BeautifulSoup(your_page .content, 'html.parser')
#
# print(soup_res)
# def covert_expired_time(posted_at):
#     datetime_object = datetime.strptime(posted_at, "%d/%m/%Y %H:%M")
#     datetime_object += timedelta(hours=-int(7))
#     return datetime_object.replace(tzinfo=timezone.utc, microsecond=0).isoformat()


def parser_url(url):
    try:
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            page = response.read()
        except Exception as e:
            print(f"RETURN: {e}", flush=True)
            return
        soup = BeautifulSoup(page, 'html5lib')

        job_title = soup.select_one("div.job-details__header > h1").text.strip()
        # job_salary= soup.select_one("div.jd-page__job-details > div > div.job-details__overview > div.svg-icon.svg-icon--rounded.svg-icon--green > div").text.strip()
        address= soup.select_one("div.jd-page__job-details > div > div.job-details__overview > div:nth-child(3) > div > span").text.strip()
        company= soup.select_one("div.jd-page__employer-overview > div > div.employer-long-overview > div.employer-long-overview__top > div.employer-long-overview__top-left > h3 > a").text.strip()
        content = str(soup.select_one("div.jd-page__job-details > div"))
        job_url = url

        job = Job(title=job_title, salary=None, content=content, company=company, address=address, url=job_url)

    except Exception as e:
        print(f"RETURN: {e}", flush=True)
        return None
    return job


def check_cached_link(link):
    if not r.exists(link):
        print(f"{link} processing ...", flush=True)
        return False
    else:
        print(f"{link} cached ...", flush=True)
        return True


def check_cached_title(title):
    if not r.exists(title):
        print(f"{title} processing ...", flush=True)
        return False
    else:
        print(f"{title} cached ...", flush=True)
        return True


def cache_job(job):
    link = job.url
    title = job.title
    r.set(link, 1)
    r.set(title, 1)
user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers={'User-Agent':user_agent,}

def get_links(link):
    try:
        request = urllib.request.Request(link, headers=headers)
        response = urllib.request.urlopen(request)
        page = response.read()
    except Exception as e:
        print(f"RETURN: {e}", flush=True)
        return
    soup = BeautifulSoup(page, 'html5lib')
    array_link = []

    urls = soup.select("div.job__body > div > h2 > a")
    for data in urls:
        link = f"{domain}{data['href']}"
        array_link.append(link)
    array_link = list(set(array_link))
    links = [link for link in array_link if not check_cached_link(link)]
    return links




def main():
    JR = JobRepository()
    end = 100
    for page in range(1, end):
        link_list_job= f"https://itviec.com/it-jobs?page={page}"
        print(link_list_job, flush=True)
        all_link = get_links(link_list_job)
        if all_link is None:
            break
        if all_link:
            for link in all_link:
                try:
                    job = parser_url(link)
                    JR.insert_jobs(title=job.title, salary=job.salary, content=job.content, company=job.company, address=job.address, url=job.url)
                    # if job and not check_cached_title(job.title):
                    #     cache_job(job)

                except Exception as e:
                    print(e, flush=True)
                    print(f"Can't parse {link}", flush=True)
                    f = open("special.txt", 'a')
                    f.write(link + '\n')
                    f.close()

        print(page)
main()

import logging

import mysql.connector
from util import load_config

Config = load_config()

conn = mysql.connector.connect(host=Config['postgres']['host'],
port=Config['postgres']['port'],
                        database=Config['postgres']['database'],
                        user=Config['postgres']['user'],
                        password=Config['postgres']['password'])


cursor = conn.cursor()
create_table_job_query = """CREATE TABLE jobs(
	id  SERIAL primary key,
	title text,
	salary text,
	content text,
	company text,
	address text,
	url text,
	isActive boolean default True
);"""
insert_job_query = """INSERT INTO jobs (title, salary, content, company, address,url)
                       VALUES  (%s,%s,%s,%s,%s,%s)"""
update_isActive_query = """UPDATE jobs SET isActive = False
                       where url not in %s """


def log_init_db(error: str):
    logging.basicConfig(filename="../tmp/init_database_log.log", filemode="a",
                        format='%(asctime)s - %(message)s', level=logging.INFO)
    logging.info(error)


class JobRepository:
    def __init__(self):
        print(cursor.execute("select * from jobs;"))

    def insert_jobs(self, title, salary, content, company, address, url):
        cursor.execute(insert_job_query, (title, salary, content, company, address, url))

    def update_isActive(self, jobid):
        cursor.execute(update_isActive_query, (jobid,))

import os
import yaml
import mechanize
from bs4 import BeautifulSoup
# import urllib2
# import cookielib


def load_config():
    """
    load config from config.yaml file
    :return:
    """
    yml_file = open(os.path.join(os.path.dirname(__file__), 'config', 'config.yaml'), 'r', encoding="utf-8")
    Config = yaml.load(yml_file, Loader=yaml.FullLoader)
    yml_file.close()
    return Config


# def login()
#     ## http.cookiejar in python3
#
#     cj = cookielib.CookieJar()
#     br = mechanize.Browser()
#     br.set_cookiejar(cj)
#     br.open("https://id.arduino.cc/auth/login/")
#
#     br.select_form(nr=0)
#     br.form['username'] = 'username'
#     br.form['password'] = 'password.'
#     br.submit()
#
#     print
#     br.response().read()

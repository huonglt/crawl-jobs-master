class Job:
    def __init__(self, id=None, title=None, salary=None, content=None, company=None, address=None, url=None,
                 is_active=None):
        self.id = id
        self.title = title
        self.salary = salary
        self.content = content
        self.company = company
        self.address = address
        self.url = url
        self.is_active = is_active
